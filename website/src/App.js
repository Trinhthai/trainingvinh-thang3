import logo from "./logo.svg";
import "./App.css";

import axios from "axios";

import React, { useEffect, useState } from "react";
function App() {
  const [text, setText] = useState("");
  const [textPass, setTextPass] = useState("");

  function checkData(data) {
    return new Promise((resolve, reject)=>{

      if(data>1){
        return resolve(data);
      }
      return reject(data);
    });
  }

  //

  useEffect(() => {
    // alert("data" + checkData());
    checkData(3).then(res=>alert('then:'+res)).catch(e=>alert('e:'+e))
  }, []);
  useEffect(() => {
    //https://api.lunchw.me/api/v1/client/user/list/rated
    // var requestOptions = {
    //   method: "GET",
    // };
    //   fetch("https://api.lunchw.me/api/v1/client/user/list/rated", requestOptions)
    //     .then((response) => response.text())
    //     .then((result) => console.log("---data----", result))
    //     .catch((error) => console.log("error", error));
    // }, []);
    // var config = {
    //   method: "get",
    //   url: "https://api.lunchw.me/api/v1/client/user/list/rated",
    //   headers: {
    //     "Content-Type": "application/x-www-form-urlencoded",
    //   },
    // };
    // axios(config)
    //   .then(function (response) {
    //     console.log(JSON.stringify(response.data));
    //   })
    //   .catch(function (error) {
    //     console.log(error);
    //   });
  }, []);

  useEffect(() => {
    //hanh dong(action)
    //props: name, color
    //state: old
    //con cho:
    // name, old, color, action, .....

    console.log("chay vao day");
    //
  }, [text]);

  return (
    <div className="App">
      <span>hello</span>
      <form
        onSubmit={() => {
          if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text)) {
            alert(
              "run" + "    username" + text + "|    password:   " + textPass
            );
            return;
          }

          alert("dinh dang email chua dung");
        }}
      >
        <label>
          username:
          <input
            type="text"
            name="username"
            value={text}
            onChange={(event) => {
              console.log("value", event);
              setText(event.target.value);
            }}
          />
        </label>

        <label>
          password:
          <input
            type="password"
            name="password"
            value={textPass}
            onChange={(event) => {
              console.log("value", event);
              setTextPass(event.target.value);
            }}
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
}

export default App;
